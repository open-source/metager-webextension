const CopyPlugin = require("copy-webpack-plugin");
const path = require("path");
const { ProvidePlugin, DefinePlugin } = require("webpack");

module.exports = (env, argv) => ({
  resolve: {
    fallback: {
      assert: false,
      crypto: require.resolve("crypto-browserify"),
      constants: false,
      vm: false,
      stream: require.resolve("stream-browserify"),
      buffer: require.resolve("buffer/"),
    },
  },
  entry: {
    app: "./build/js/app.js",
    removeUnusedContent: "./build/js/contentScripts/removeUnusedContent.js",
    settingsPage: "./build/js/contentScripts/settings.js",
    tokenauthorization: "./build/js/contentScripts/tokenauthorization.js",
    messaging: "./build/js/contentScripts/messaging.js",
    keys: "./build/js/contentScripts/keys.js"
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "js"),
  },
  devtool: "source-map",
  watch: true,
  watchOptions: {
    ignored: ["/node_modules"],
  },
  plugins: [
    new ProvidePlugin({
      Buffer: ["buffer", "Buffer"],
      process: "process/browser",
    }),
    new CopyPlugin({
      patterns: [{
        from: __dirname + "/build/manifest.json",
        to: __dirname + "/manifest.json",
        transform(content, path) {
          return buildManifest(content, argv.mode);
        }
      }]
    }),
    new DefinePlugin({
      BROWSER: JSON.stringify(process.env.BROWSER)
    }),
  ]
});

function buildManifest(content, mode = "production") {
  let manifest = JSON.parse(content);

  let browser_env = process.env.BROWSER;
  if (browser_env) {
    if (mode == "development") {
      // Add access to localhost for development environments
      manifest.content_scripts[0].matches.push("http://localhost/*");
      manifest.content_scripts[1].matches.push("http://localhost/*");
      manifest.content_scripts[2].matches.push(
        'http://localhost/meta/settings',
        'http://localhost/*/meta/settings',
        'http://localhost/meta/settings?*',
        'http://localhost/*/meta/settings?*'
      );
      manifest.content_scripts[3].matches.push(
        'http://localhost/meta/meta.ger3?*',
        'http://localhost/*/meta/meta.ger3?*'
      );
      manifest.content_scripts[4].matches.push(
        'http://localhost/keys/*',
        'http://localhost/*/keys/*'
      );
      manifest.host_permissions.push("http://localhost/*", "http://localhost:8080/*");
    }

    if (browser_env == "CHROME") {
      // Chrome does not support background_scripts
      // We need to convert to service_worker
      manifest.background.service_worker = manifest.background.scripts[0];
      delete manifest.background.scripts;
      // browser_specific_settings not supported in chrome
      delete manifest.browser_specific_settings
      manifest.chrome_settings_overrides.search_provider.favicon_url = "https://metager.de/favicon.ico";
    }
  }

  return JSON.stringify(manifest);
}
