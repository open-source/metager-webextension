#!/bin/bash

# Verify that docker is installed
if ! command -v docker &> /dev/null
then
    echo "docker seems to not be installed on your system";
    exit 1;
fi

CURRENT_DIR=`dirname $BASH_SOURCE`;

# Export variables to this environment
source $CURRENT_DIR/.env

docker run -it --rm -v $CURRENT_DIR/../../:/data --workdir=/data node:$NODE_VERSION npm run dev
