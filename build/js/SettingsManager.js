import { Cookie } from "./Cookie";

if (typeof browser == "undefined") {
  var browser = chrome;
}

/**
 * Handles changes in the search settings when the user
 * makes them on our Website. It will catch the regular responses 
 * which would store the settings in form of cookies and stores
 * them within this web-extension instead.
 * It will also generate DeclarativeNetRequest Rules to attach
 * those settings to requests to our server in form of custom http headers
 * 
 * Neither the Cookie Request Header nor the Set-Cookie Response header is modified.
 */
export class SettingsManager {
  initialized = null;
  _ignored_settings = ["key", "cost", "tokens", "decitokens", "metager_session", "keymanagerSession", "auth_verification", "_auth_verification"];

  store_settings = true;
  settings = {};

  urls = [];
  origins = [];
  hosts = [];

  constructor() {
    browser.runtime.getManifest().host_permissions.forEach((value, index) => {
      this.urls.push(value);
      let url = new URL(value);
      this.hosts.push(url.hostname);
      this.origins.push(url.origin);
    });
  }

  async init() {
    if (this.initialized != null) return this.initialized;
    this.initialized = browser.storage.sync.get({ settings_store: null, settings_settings: {} })
      .then(async (stored_settings) => {
        this.settings = stored_settings.settings_settings;
        let settings_store_permission_overwrite = await this._checkPermissions();
        if (settings_store_permission_overwrite == false) {
          this.store_settings = false;
        } else if (stored_settings.settings_store == null) {
          this.store_settings = true;
          return this._enable();
        } else {
          this.store_settings = stored_settings.settings_store;
          return this._enable();
        }
        return;
      })
      .then(() => this.updateSettingRule())
      .then(async () => {
        this.initialized = true;
        return this
      });
    return this.initialized;
  }

  /**
   * Merges new settings into existing ones and submits them
   * into storage
   * 
   * @param {object} settings 
   */
  async set(settings) {
    await this.init();
    this.settings = { ...this.settings, ...settings };
    this._ignored_settings.forEach(ignored_setting => {
      delete this.settings[ignored_setting];
    });
    return this.sync().then(() => this.updateSettingRule());
  }

  /**
   * Removes sepecified setting key 
   * and syncs settings
   * 
   * @param {string} setting_key 
   */
  async remove(setting_key) {
    await this.init();
    if (this._ignored_settings.includes(setting_key)) return;
    delete this.settings[setting_key];
    return this.sync().then(() => this.updateSettingRule());
  }

  /**
   * Syncs current setting state with storage
   */
  async sync() {
    return browser.storage.sync.set({ settings_store: this.store_settings, settings_settings: this.settings });
  }

  async handleCookieChange(changeInfo) {
    await this.init();
    if (changeInfo.removed) return;  // Do not handle removed cookies
    if (!this.store_settings) return; // Do not handle cookies if we're currently not storing settings in extension
    if (this._ignored_settings.includes(changeInfo.cookie.name)) return; // Do not handle ignored settings
    let protocol = changeInfo.cookie.secure ? "https" : "http";

    return browser.cookies.remove({
      name: changeInfo.cookie.name,
      url: `${protocol}://${changeInfo.cookie.domain}${changeInfo.cookie.path}`
    });
  }

  /**
   * Enable/Disable Settingsmanager when permissions
   * are revoked or granted
   * 
   * @param {*} permissions 
   * @returns 
   */
  async _checkPermissions() {
    let permissionsToRequest = {
      origins: [
        "https://metager.org/*",
        "https://metager.de/*",
      ]
    };

    return browser.permissions.contains(permissionsToRequest);
  }

  async handleStorageChange(changes) {
    await this.init();
    let updated = false;
    if (
      changes.hasOwnProperty("settings_store") &&
      typeof changes.settings_store.newValue == "boolean" &&
      changes.settings_store.newValue != changes.settings_store.oldValue
    ) {
      updated = true;
      let new_value = changes.settings_store.newValue;
      if (new_value == true) {
        // Setting storage now enabled
        await this._enable();
      } else if (new_value == false) {
        // Setting storage now disabled. Put all settings into browser cookie storage
        await this._disable();
      }
    }
    if (changes.hasOwnProperty("settings_settings")) {
      updated = true;
      this.settings = changes.settings_settings.newValue;
    }
    if (updated) {
      return this.updateSettingRule();
    }
  }

  /**
   *
   * @param {*} details
   */
  async handleResponseHeaders(details) {
    if (!this.store_settings) return details.responseHeaders;
    await this.init();
    if (details.hasOwnProperty("responseHeaders")) {
      let settings_changed = false;
      let cookies_to_remove = [];
      for (const header of details.responseHeaders) {
        if (header.name.match(/set-cookie/i)) {
          let cookie_responses = header.value.split("\n");
          for (let cookie of cookie_responses) {
            let response_cookie = new Cookie(cookie);
            if (this._ignored_settings.includes(response_cookie.key)) continue;
            if (response_cookie.expired) {
              delete this.settings[response_cookie.key];
              // Remove cookie from browser
              cookies_to_remove.push(response_cookie);
            } else {
              this.settings[response_cookie.key] = decodeURIComponent(
                response_cookie.value
              );
              cookies_to_remove.push(response_cookie);
            }
          }
          settings_changed = true;
        }
      }
      if (settings_changed) {
        await this.sync().then(() => {
          let removal_promises = [];
          for (let cookie of cookies_to_remove) {
            removal_promises.push(Cookie.REMOVE_COOKIE(cookie));
          }
          return Promise.all(removal_promises);
        });
      }
      return;
    }
  }

  /**
   * Creates a declarativeNetRequest Rule
   * for attaching the settings to requests to our website
   */
  async updateSettingRule() {
    if (!browser.hasOwnProperty("declarativeNetRequest")) return;
    if (!this.store_settings) {
      // Remove all rules if webextension does not store settings
      return browser.declarativeNetRequest.updateDynamicRules({
        addRules: [],
        removeRuleIds: [1]
      });
    }
    let headers = [];
    let ignore_settings = ["key", "cost", "tokens", "decitokens"];
    for (let key in this.settings) {
      if (ignore_settings.includes(key)) continue;
      headers.push({
        header: key,
        value: this.settings[key],
        operation: "set"
      });
    }
    let rules = [];

    // Add Rule for web extension version
    rules.push({
      id: 6,
      condition: {
        requestDomains: this.hosts,
        resourceTypes: ["main_frame", "sub_frame", "xmlhttprequest"]
      },
      action: {
        type: "modifyHeaders",
        requestHeaders: [{
          header: "Mg-Webext",
          value: chrome.runtime.getManifest().version,
          operation: "set"
        }]
      }
    });
    if (headers.length > 0) {
      rules.push({
        id: 1,
        condition: {
          requestDomains: this.hosts,
          resourceTypes: ["main_frame", "sub_frame", "xmlhttprequest"]
        },
        action: {
          type: "modifyHeaders",
          requestHeaders: headers
        }
      });
    }
    return browser.declarativeNetRequest.updateDynamicRules({
      addRules: rules,
      removeRuleIds: [1, 6]
    });
  }

  async _enable() {
    this.store_settings = true;
    if (!browser.cookies) return;
    // First fetch all cookies from metager.org

    // Fetch cookies from all origins
    let cookie_promises = [];
    this.origins.forEach(origin => {
      let origin_url = new URL(origin);
      cookie_promises.push(browser.cookies.getAll({ url: origin }).then(cookies => {
        let cookie_delete_promises = [];
        for (let cookie of cookies) {
          if (this._ignored_settings.includes(cookie.name)) continue;
          this.settings[cookie.name] = cookie.value;
          cookie_delete_promises.push(browser.cookies.remove({ name: cookie.name, url: origin }));
        }
        return Promise.all(cookie_delete_promises);
      }).catch(error => console.error(error)));
    });

    return Promise.all(cookie_promises).then(() => this.sync());
  }

  async _disable() {
    this.store_settings = false;
    if (!browser.cookies) return;
    let create_cookies_promises = [];

    let expiration = new Date();
    expiration.setFullYear(expiration.getFullYear() + 1);
    for (let setting_key in this.settings) {
      this.origins.forEach(origin => {
        let origin_url = new URL(origin);
        create_cookies_promises.push(browser.cookies.set({
          url: origin,
          name: setting_key,
          value: this.settings[setting_key],
          expirationDate: Math.round(expiration.getTime() / 1000),
          secure: origin_url.protocol != "http:"
        }).catch(error => console.error(error)));
      });
    }

    return Promise.all(create_cookies_promises).then(async () => this.updateSettingRule());
  }
}

