if (typeof browser == "undefined") {
    var browser = chrome;
}

/**
 * Parses a "Set-Cookie" header coming from a browser
 * to give easy access to all necessary parameters of it
 */
export class Cookie {
    key = null;
    value = null;
    expired = false;
    constructor(cookieString) {
        // Return an empty object if cookieString
        // is empty
        if (cookieString === "")
            throw new Error("Cannot parse cookie string " + cookieString);

        // Get each individual key-value pairs
        // from the cookie string
        // This returns a new array
        let pairs = cookieString.split(";");

        // Separate keys from values in each pair string
        // Returns a new array which looks like
        // [[key1,value1], [key2,value2], ...]
        let splittedPairs = pairs.map(cookie => cookie.split("="));


        let expiration = null;
        for (const [index, pair] of splittedPairs.entries()) {
            if (index == 0) {
                this.key = pair[0].trim();
                this.value = decodeURIComponent(pair[1].trim());
            } else {
                let key = pair[0].trim();
                if (key.match(/^expires$/i) && this.expiration == null) {
                    expiration = new Date(pair[1].trim());
                } else if (key.match(/^max-age$/i)) {
                    let maxAge = parseInt(pair[1].trim());
                    if (maxAge == 0) {
                        this.expired = true;
                    } else {
                        expiration = new Date((new Date()).getTime() + maxAge * 1000);
                    }
                }
            }
        };

        if (expiration != null && Date.now() > expiration.getTime()) {
            this.expired = true;
        }
    }

    /**
     * 
     * @param {Cookie} cookie 
     */
    static async REMOVE_COOKIE(cookie) {
        let response_promises = [];
        response_promises.push(browser.cookies.remove({
            url: "https://metager.de/",
            name: cookie.key
        }));
        response_promises.push(browser.cookies.remove({
            url: "https://metager.org/",
            name: cookie.key
        }));
        return Promise.all(response_promises);
    }
}