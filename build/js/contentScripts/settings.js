/**
 * Handle the change of Search Settings on https://metager.org/meta/settings and https://metager.de/meta/settings (localization)
 * 
 * Depending on the search focus and which setting is changed by the user by using any of the
 * input fields on the settings page the resulting configuration can be different depending on which
 * settings are already defined.
 * 
 * Since we do not want to have to parse which settings can be changed at any given point on our server and another time in the webextension
 * which could lead to inconsistencies this script will send any user interaction to change the search settings to our server as it would 
 * normally do but accept application/json as response so we can parse the server response which will contain the settings that are changed
 * by that last user action and forward it to the background script of this extension so it can make the setting changes in local storage.
 */
(() => {
    if (typeof browser == "undefined") {
        var browser = chrome;
    }
    /**
     * Prevent regular form submission for the settings
     * and handle the requests so we can make the changes in the
     * webextension storage instead
     */
    document.querySelectorAll("form").forEach(form => {
        form.addEventListener("submit", e => {
            e.preventDefault();
            submitForm(form);
        });
    });

    /**
     * Prevent existing form submissions when a select value changes
     * and handle the request so we can make the changes in the
     * webextension storage instead
     */
    document.querySelectorAll("select").forEach(select => {
        let element = select.cloneNode(true);
        select.parentNode.replaceChild(element, select);
        element.addEventListener("change", e => {
            submitForm(element.form);
        })
    });

    /**
     * When the user has setup a MetaGer key there is an option to remove the
     * key via an anchor.
     * We need to handle a click on that button to remove this specific setting
     * from our webextension storage.
     */
    document.querySelectorAll("a").forEach(anchor => {
        anchor.addEventListener("click", e => {
            try {
                let url = new URL(anchor.href);
                if (url.pathname.match(/\/(.*\/)?keys\/key\/remove/)) {
                    e.preventDefault(); // Prevent direct loading of page
                    browser.runtime.sendMessage({ type: "settings_remove", setting_key: "key" }).then(() => {
                        document.location.href = url;
                    })
                }
            } catch (error) { }
        })
    });

    /**
     * Handle search setting changes for MetaGer which the user requests;
     * default form submission will be prevented. Instead we will query our server
     * for a machine readable response which contains the settings that changed with this
     * request so we can handle the setting changes in the webextensions background script
     * 
     * @param {HTMLFormElement} form 
     */
    function submitForm(form) {
        let form_action = form.action;

        const form_data = new URLSearchParams();
        for (const pair of new FormData(form)) {
            form_data.append(pair[0], pair[1]);
        }

        fetch(form_action, {
            method: "POST",
            headers: {
                "Accept": "application/json"
            },
            credentials: "include",
            redirect: "follow",
            body: form_data
        })
            .then(response => response.json())
            .then(async response => {
                for (let remove of response.remove) {
                    await browser.runtime.sendMessage({ type: "settings_remove", setting_key: remove });
                }
                if (Object.keys(response.set).length > 0) {
                    return browser.runtime.sendMessage({ type: "settings_set", settings: response.set })
                        .then(answer => {
                            return response.redirect
                        });
                } else {
                    return response.redirect;
                }
            }).then((url) => {
                if (url) {
                    if (!url.match(/#.*/) && form.id) {
                        url += "#form.id";
                    }
                    document.location.href = url;
                }
                document.location.reload();
            })
            .catch(error => {
                //form.submit();
                console.log(error);
            })
    }
})();