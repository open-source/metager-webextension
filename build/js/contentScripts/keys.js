/**
 * If the user is using a MetaGer key the path /keys/*
 * will contain a "Logout" anchor link which will remove the
 * MetaGer key from the browser.
 * Since this extension is storing the key instead we will catch Logout
 * requests and forward them to the extension so the key can be removed
 * from extension storage.
 */
(() => {
    if (typeof browser == "undefined") {
        browser = chrome;
    }
    let logout_link_anchor = document.getElementById("key-remove");
    if (!logout_link_anchor) return;
    logout_link_anchor.addEventListener("click", e => {
        try {
            let url = new URL(logout_link_anchor.href);
            e.preventDefault(); // Prevent direct loading of page
            browser.runtime.sendMessage({ type: "settings_remove", setting_key: "key" }).then(() => {
                document.location.href = url;
            })
        } catch (error) { }
    })
})();